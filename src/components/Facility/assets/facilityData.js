import office from './office.svg'
import factorie from './factorie.svg'
import hotel from './hotel.svg'
import house from './house.svg'
import largeBuilding from './large-building.svg'
import pool from './pool.svg'
import restaurant from './restaurant.svg'
import shoppingCenter from './shopping-center.svg'

export const facilityData = [
  {
    src: house,
    name: 'Дома'
  },
  {
    src: largeBuilding,
    name: 'Большие сооружения'
  },
  {
    src: factorie,
    name: 'Заводы/Склады'
  },
  {
    src: office,
    name: 'Офисы'
  },
  {
    src: restaurant,
    name: 'Рестораны'
  },
  {
    src: pool,
    name: 'Бассейны'
  },
  {
    src: shoppingCenter,
    name: 'Торговые центры'
  },
  {
    src: hotel,
    name: 'Гостиницы'
  }
]
