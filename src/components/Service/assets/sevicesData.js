import konsult from './konsult.svg'
import card from './card.svg'
import fen from './fen.svg'
import cross from './cross.svg'
import list from './list.svg'
import note from './note.svg'

export const services = [
  {
    src: konsult,
    desc: 'Консультации'
  },
  {
    src: list,
    desc: 'Проектирование'
  },
  {
    src: fen,
    desc: 'Монтаж'
  },
  {
    src: card,
    desc: 'Продажа оборудования'
  },
  {
    src: note,
    desc: 'Сметный расчёт'
  },
  {
    src: cross,
    desc: 'Техническое обслуживание'
  },
]
